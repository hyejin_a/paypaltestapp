namespace :paypal do 

  # Update paypal status of payments having pending paypal status.
  #
  # Invoke by:
  #
  #   rake paypal:check_payments

  desc "Update paypal status of payments having pending paypal status."
  task :check_payments => :environment do |t, args|

    puts ">>> Starting task check_payments."
    Payment.paypal_pending.each do |payment|
      old_status = payment.paypal_status
      payment.update(paypal_status: EXPRESS_GATEWAY.transaction_details(payment.paypal_transaction_id).params["PaymentTransactionDetails"]["PaymentInfo"]["PaymentStatus"])
      puts "Paypal status payment #{payment.paypal_transaction_id} has changed to #{payment.paypal_status}" if payment.paypal_status != old_status
    end
    puts ">>> Task check_payments finished."

  end
  
end
