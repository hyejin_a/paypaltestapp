module ApplicationHelper

  def spinner_with_text
    "<i class='fa fa-spinner fa-spin'></i> #{I18n.t('titles.loading')}".html_safe
  end

  def spinner
    "<i class='fa fa-spinner fa-spin'></i>"
  end

  def text_only
    I18n.t('titles.loading') + "..."
  end

end
