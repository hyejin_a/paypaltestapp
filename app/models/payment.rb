class Payment < ActiveRecord::Base

  has_one :cart

  INCOMPLETE = 'incomplete'   # Initial state
  SUCCESS = 'success'         # Payment wa successful
  # CANCELLED = 'cancelled'     # User chose not to confirm payment in final step.
  FAILED = 'failed'     # User chose not to confirm payment in final step.

  scope :incomplete, -> { where(state: INCOMPLETE) }
  scope :success, -> { where(state: SUCCESS) }
  # scope :cancelled, -> { where(state: CANCELLED) }
  scope :failed, -> { where(state: FAILED) }
  scope :paypal_pending, -> { where(paypal_status: "Pending") }
  scope :paypal_completed, -> { where(paypal_status: "Completed") }

  attr_reader :sucess, :result_message

  def purchase
    response = EXPRESS_GATEWAY.purchase(cart.price_in_cents, express_purchase_options)
    # cart.update_attribute(:purchased_at, Time.now) if response.success?
    update(paypal_fee: response.params["fee_amount"], 
      paypal_status: response.params["PaymentInfo"]["PaymentStatus"],
      paypal_transaction_id: response.params["transaction_id"],
      state: response.success? ? SUCCESS : FAILED)
    cart.state_paid if response.success?
    @result_message = response.success? ? 'Your payment has been received, thank you!' : response.message
    @success = response.success?
  end

  def express_token=(token)
    self[:express_token] = token
    if new_record? && !token.blank?
      # you can dump details var if you need more info from buyer
      details = EXPRESS_GATEWAY.details_for(token)
      self.express_payer_id = details.payer_id
    end
  end

  private

  def express_purchase_options
    {
      :ip => ip,
      :token => express_token,
      :payer_id => express_payer_id
    }
  end
end
