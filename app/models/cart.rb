class Cart < ActiveRecord::Base

  INCOMPLETE = 'incomplete'   # Initial state
  PAID = 'paid'         # Cart turned into confirmed order after successful payment

  has_many :items
  belongs_to :payment

  # scope :open, -> { joins("join payments on payments.id = carts.payment_id").where("payments.state = ? ", Payment::INCOMPLETE) }
  scope :open, -> { joins("join payments on payments.id = carts.payment_id").where("carts.state = ? ", INCOMPLETE) }

  def self.mine
    Cart.order("id desc").where(payment_id: nil).first || Cart.order("id desc").open.first || Cart.create
  end

  def price
    items.pluck(:price).sum
  end

  def price_in_cents
    (price * 100).to_i
  end

  def state_paid
    update(state: PAID)
  end

end
