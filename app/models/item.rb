class Item < ActiveRecord::Base

  validates :price, :name, presence: true

	# def self.total_price
	# 	sum(:price)
	# end

  def price_in_cents
    (price * 100).to_i
  end

private

end
