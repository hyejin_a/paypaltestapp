class CartsController < ApplicationController
  before_action :set_cart, only: [:show, :update, :destroy, :checkout, :confirm_checkout]

  def index
    @carts = Cart.all
  end

  def show
  end

  def new
    @cart = Cart.new
  end

  def edit
    @cart = Cart.find_by(id: params[:id]) || Cart.mine
  end

  def create
    @cart = Cart.create(cart_params)
    if @cart.valid?
      redirect_to carts_url, notice: 'Cart was successfully created.'
    else
      render :new
    end
  end

  def update
    respond_to do |format|
      if @cart.update(cart_params)
        redirect_to carts_url, notice: 'Cart was successfully updated.'
      else
        render :edit
      end
    end
  end

  def destroy
    @cart.destroy
    redirect_to carts_url, notice: 'Cart was successfully destroyed.'
  end

  def checkout
    response = EXPRESS_GATEWAY.setup_purchase(@cart.price_in_cents,
      ip: request.remote_ip,
      return_url: confirm_checkout_cart_url(@cart),
      cancel_return_url: edit_cart_url(@cart),
      currency: "AUD",
      allow_guest_checkout: true,
      items: paypal_items)
    if response.token.nil?
      # The setup failed, check response.message
      flash.alert = response.message
      render 'edit'
    else
      redirect_to EXPRESS_GATEWAY.redirect_url_for(response.token)
    end
  end

  def confirm_checkout
    details = EXPRESS_GATEWAY.details_for(params[:token])
    @payment = @cart.payment || Payment.create(express_token: params[:token],
        first_name: details.params["first_name"],
        last_name: details.params["last_name"],
        amount: details.params["order_total"].to_f,
        address: details.params["street1"])
    @cart.update(payment: @payment) if @cart.payment.nil?
  end

private

  # Use callbacks to share common setup or constraints between actions.
  def set_cart
    @cart = Cart.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def cart_params
    params.fetch(:cart, {})
  end

  def paypal_items
    @cart.items.each_with_object([]) do |item, a|
      a << { name: item.name, description: item.name, quantity: 1, amount: item.price_in_cents }
    end
  end

end
