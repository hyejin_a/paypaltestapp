class ItemsController < ApplicationController

  before_action :set_item, only: [:show, :edit, :update, :destroy, :add_to_cart, :remove_from_cart]

  def index
    @items = Item.all
  end

  def show
  end

  def new
    @item = Item.new
  end

  def edit
  end

  def create
    @item = Item.new(item_params)

    if @item.save
      redirect_to items_path, notice: 'Product was successfully created.'
    else
      render :new
    end
  end

  def update
    if @item.update(item_params)
      redirect_to items_path, notice: 'Item was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @item.destroy
    redirect_to items_path, notice: 'Item was successfully destroyed.'
  end

  def add_to_cart
    cart = Cart.mine
    cart.items << @item
    redirect_to edit_cart_path(cart)
  end

  def remove_from_cart
    cart = Cart.mine
    cart.items = cart.items - [@item]
    cart.save
    redirect_to edit_cart_path(cart)
  end

private

    def set_item
      @item = Item.find(params[:id])
    end

    def item_params
      params.require(:item).permit(:name, :price)
    end
end
