class PaymentsController < ApplicationController
  before_action :set_payment, only: [:show, :edit, :update, :destroy]

  def index
    @payments = Payment.all
  end

  def show
    # byebug
    @paypal_details = EXPRESS_GATEWAY.transaction_details(@payment.paypal_transaction_id).params rescue nil
  end

  def new
  end

  def edit
  end

  def create
  end

  def update
    @payment.update(payment_params)
    @payment.purchase
end

  def destroy
    @payment.destroy
    respond_to do |format|
      format.html { redirect_to payments_url, notice: 'Payment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_payment
      @payment = Payment.find(params[:id])
    end

    def payment_params
      params.require(:payment).permit(:ip, :express_token, :express_payer_id)
    end
end
