class AddPaypalFeeToPayment < ActiveRecord::Migration
  def change
    add_column :payments, :paypal_fee, :float
  end
end
