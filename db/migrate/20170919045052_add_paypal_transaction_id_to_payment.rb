class AddPaypalTransactionIdToPayment < ActiveRecord::Migration
  def change
    add_column :payments, :paypal_transaction_id, :string
    add_column :payments, :paypal_status, :string
  end
end
