class AddStateToCart < ActiveRecord::Migration
  def change
    add_column :carts, :state, :string, default: 'incomplete'
  end
end
