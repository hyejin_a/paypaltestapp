class AddPaymentToCart < ActiveRecord::Migration
  def change
    add_reference :carts, :payment, index: true, foreign_key: true
  end
end
