class AddFieldsToPayment < ActiveRecord::Migration
  def change
    add_column :payments, :first_name, :string
    add_column :payments, :last_name, :string
    add_column :payments, :address, :string
    add_column :payments, :amount, :float
  end
end
